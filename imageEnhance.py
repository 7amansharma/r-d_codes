import cv2
import numpy as np
import time

cv2.namedWindow("original",cv2.WINDOW_NORMAL)
# cv2.namedWindow("iluImage",cv2.WINDOW_NORMAL)
# cv2.namedWindow("i2",cv2.WINDOW_NORMAL)
# cv2.namedWindow("i3",cv2.WINDOW_NORMAL)
cv2.namedWindow("final",cv2.WINDOW_NORMAL)


img=cv2.imread("/home/aman/Downloads/picture-21.jpg")
t1=time.time()
img_hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
hue,sat,val=cv2.split(img_hsv)

iluImage=np.max(img,axis=2)
strel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
iluImage=cv2.morphologyEx(iluImage,cv2.MORPH_CLOSE,strel)
iluImage_scaled=iluImage.astype('float')/255.0
i1=iluImage_scaled
iluImage_scaled_mean=np.mean(iluImage_scaled)
lambda_=10 + ((1-iluImage_scaled_mean)/iluImage_scaled_mean)
i2=(2.0/np.pi)*np.arctan(lambda_*iluImage_scaled)
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
i3=clahe.apply(iluImage)
i3=i3.astype("float")/255.0
#print(np.max(i1),np.max(i2),np.max(i3))
wb1=np.exp(-np.power(i1-0.5,2)/(2*0.0625))
wb2=np.exp(-np.power(i2-0.5,2)/(2*0.0625))
wb3=np.exp(-np.power(i3-0.5,2)/(2*0.0625))

hue=hue.astype('float')/255.0
sat=sat.astype('float')/255.0
temp=(1+np.cos( 2* hue + (np.pi/180.0)*250 )*sat)
wc1=i1*temp
wc2=i2*temp
wc3=i3*temp

w1=wb1*wc1
w2=wb2*wc2
w3=wb3*wc3

sum_w=w1+w2+w3
w1_norm=w1/sum_w
w2_norm=w2/sum_w
w3_norm=w3/sum_w


# f_l=[]
# for i in xrange(6):
#     for j in xrange(3):


 # generate Gaussian pyramid for A
#G = A.copy()
gpW1 = [w1_norm]
#print(w1_norm.shape)
for i in xrange(6):
    w1_norm = cv2.pyrDown(w1_norm)
    #print(w1_norm.shape)
    gpW1.append(w1_norm)

gpW2 = [w2_norm]
for i in xrange(6):
    w2_norm = cv2.pyrDown(w2_norm)
    #print(w2_norm.shape)
    gpW2.append(w2_norm)

gpW3 = [w3_norm]
for i in xrange(6):
    w3_norm = cv2.pyrDown(w3_norm)
    gpW3.append(w3_norm)


gpI1 = [i1]
for i in xrange(6):
    #print(gpI1[i].shape)
    i1 = cv2.pyrDown(i1)
    gpI1.append(i1)

gpI2 = [i2]
for i in xrange(6):
    i2 = cv2.pyrDown(i2)
    gpI2.append(i2)

gpI3 = [i3]
for i in xrange(6):
    i3 = cv2.pyrDown(i3)
    gpI3.append(i3)

#print("\n")
lpI1 = [gpI1[5]]
#print(gpI1[5].shape)
for i in xrange(5,0,-1):
    GE = cv2.pyrUp(gpI1[i],dstsize=(gpI1[i-1].shape[1],gpI1[i-1].shape[0]))
    L = cv2.subtract(gpI1[i-1],GE)
    #print(L.shape)
    lpI1.append(L)

lpI2 = [gpI2[5]]
for i in xrange(5,0,-1):
    GE = cv2.pyrUp(gpI2[i],dstsize=(gpI2[i-1].shape[1],gpI2[i-1].shape[0]))
    L = cv2.subtract(gpI2[i-1],GE)

    lpI2.append(L)

lpI3 = [gpI3[5]]
for i in xrange(5,0,-1):
    GE = cv2.pyrUp(gpI3[i],dstsize=(gpI3[i-1].shape[1],gpI3[i-1].shape[0]))
    L = cv2.subtract(gpI3[i-1],GE)
    lpI3.append(L)

f_l=[]
for i in xrange(6):
    #print(i,len(gpI1),len(gpI2),len(gpI3),len(gpW1),len(gpW2),len(gpW3))
    #print(lpI1[5-i].shape,gpW1[i].shape,lpI2[5-i].shape,gpW2[i].shape,lpI3[5-i].shape,gpW3[i].shape)
    temp=np.multiply(lpI1[5-i],gpW1[i]) + np.multiply(lpI2[5-i],gpW2[i]) + np.multiply(lpI3[5-i],gpW3[i])
    f_l.append(temp)
    #print(temp.shape)

final_iluImage=f_l[0]
size=(f_l[0].shape[1],f_l[0].shape[0])
for i in xrange(1,6):
    final_iluImage=final_iluImage + cv2.resize(f_l[i],size)
#print(iluImage_scaled.shape)
reflectance_img=np.divide(img.astype("float")/255.0,np.reshape( iluImage_scaled,(iluImage_scaled.shape[0],iluImage_scaled.shape[1],1)))

final_img=np.multiply(reflectance_img,final_iluImage.reshape((iluImage_scaled.shape[0],iluImage_scaled.shape[1],1)))

print(time.time()-t1)
cv2.imshow("original",img)
cv2.imshow("final",final_img)
cv2.waitKey(0)
#ACCESS_FAST