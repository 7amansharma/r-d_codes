import cv2
import numpy as np
import time

cv2.namedWindow("original",cv2.WINDOW_NORMAL)
cv2.namedWindow("onlyface",cv2.WINDOW_NORMAL)
cv2.namedWindow("onlyface2",cv2.WINDOW_NORMAL)
cv2.namedWindow("diff",cv2.WINDOW_NORMAL)
cv2.namedWindow("onlyeye",cv2.WINDOW_NORMAL)
cv2.namedWindow("eyediff",cv2.WINDOW_NORMAL)
cam=cv2.VideoCapture(0)
list_face=[1,2]
last_coord=[20,20,20,20]
counter=0
last_eye_point_var=0
ex=-1
while(1):
    ret,img=cam.read()
    #time.sleep(1)
    ret2,img2=cam.read()
    print(type(img))
    t1=time.time()
    img_hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
    hue,sat,val=cv2.split(img_hsv)
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray2=cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)

    #gray=cv2.equalizeHist(gray)
    #gray2=cv2.equalizeHist(gray2)

    #iluImage=np.max(img,axis=2)
    # strel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(9,9))
    # iluImage=cv2.morphologyEx(iluImage,cv2.MORPH_CLOSE,strel)
    # iluImage_scaled=iluImage.astype('float')/255.0
    # i1=iluImage_scaled
    # iluImage_scaled_mean=np.mean(iluImage_scaled)

    # reflectance_img=np.divide(img.astype("float")/255.0,np.reshape( iluImage_scaled,(iluImage_scaled.shape[0],iluImage_scaled.shape[1],1)))
    # print(reflectance_img.astype("uint8").dtype)
    # gray2=cv2.cvtColor((reflectance_img*255).astype("uint8"),cv2.COLOR_BGR2GRAY)
    # gray2=cv2.equalizeHist(gray2)
    #final_img=np.multiply(reflectance_img,final_iluImage.reshape((iluImage_scaled.shape[0],iluImage_scaled.shape[1],1)))
    face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_eye.xml')
    #img = cv2.imread('me.jpg')
    #print(img.shape)
    #cv2.imshow('img',img)
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)`
    #gray=gray2
    #print(gray.shape)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    print(faces)
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x+20,y+20),(x+w-20,y+h-20),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_gray2 = gray2[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        list_face[0]=gray[y+20:y+h-20, x+20:x+w-20].copy()
        list_face[1]=gray2[y+20:y+h-20, x+20:x+w-20].copy()
        counter = 0 if counter else 1
        #last_coord=[x,y,w,h]
        #print("eyes",eyes)
        try:
            #print("eyes.shape",eyes.shape)
            #print("eyes[0][:]",eyes[0][:])
            # for (ex,ey,ew,eh) in eyes:
            #     cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
            #     break
            
            for i in eyes:
                if(ex==-1):
                    ex=i[0]
                if(abs(i[0] - ex) < 20 ):
                    (ex,ey,ew,eh)=i
                    cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
                    last_eye_point_var=ex
                    break
                else:
                    pass
        except:
            pass
    print(time.time()-t1)
    cv2.imshow("original",img)
    #cv2.rectangle
    cv2.imshow("onlyface",list_face[0])
    try:

        cv2.imshow("onlyface2",list_face[1])
        x=(cv2.equalizeHist(roi_gray[ey:ey+eh, ex:ex+ew]) )#.astype("float")
        strel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
        x=cv2.morphologyEx(x,cv2.MORPH_CLOSE,strel)

        y=(cv2.equalizeHist(roi_gray2[ey:ey+eh, ex:ex+ew]) )#.astype("float")
        strel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
        y=cv2.morphologyEx(y,cv2.MORPH_CLOSE,strel)
        cv2.imshow("eyediff", (cv2.absdiff(x,y)> 1 ).astype("float"))
        #x=cv2.morphologyEx(x,cv2.MORPH_OPEN,strel)
        cv2.imshow("onlyeye",x)
        cv2.imshow("diff",(cv2.absdiff(list_face[1],list_face[0]) > 15).astype("float") )
    except:
        pass


    #cv2.imshow("equalizeHistOFreflectance_img",gray2)
    if(cv2.waitKey(30)>0):
        break
#ACCESS_FAST