import cv2
import numpy as np
import time

cv2.namedWindow("original",cv2.WINDOW_NORMAL)
# cv2.namedWindow("iluImage",cv2.WINDOW_NORMAL)
cv2.namedWindow("equalizeHistOFreflectance_img",cv2.WINDOW_NORMAL)
cv2.namedWindow("equalizeHist",cv2.WINDOW_NORMAL)
cv2.namedWindow("reflectance_img",cv2.WINDOW_NORMAL)
cam=cv2.VideoCapture(0)
while(1):
    ret,img=cam.read()
    print(type(img))
    t1=time.time()
    img_hsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
    hue,sat,val=cv2.split(img_hsv)
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray=cv2.equalizeHist(gray)

    iluImage=np.max(img,axis=2)
    # strel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(9,9))
    # iluImage=cv2.morphologyEx(iluImage,cv2.MORPH_CLOSE,strel)
    iluImage_scaled=iluImage.astype('float')/255.0
    i1=iluImage_scaled
    iluImage_scaled_mean=np.mean(iluImage_scaled)

    reflectance_img=np.divide(img.astype("float")/255.0,np.reshape( iluImage_scaled,(iluImage_scaled.shape[0],iluImage_scaled.shape[1],1)))
    print(reflectance_img.astype("uint8").dtype)
    gray2=cv2.cvtColor((reflectance_img*255).astype("uint8"),cv2.COLOR_BGR2GRAY)
    gray2=cv2.equalizeHist(gray2)
    #final_img=np.multiply(reflectance_img,final_iluImage.reshape((iluImage_scaled.shape[0],iluImage_scaled.shape[1],1)))
    face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_eye.xml')
    #img = cv2.imread('me.jpg')
    #print(img.shape)
    #cv2.imshow('img',img)
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)`
    #gray=gray2
    print(gray.shape)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
    print(time.time()-t1)
    cv2.imshow("original",img)
    cv2.imshow("equalizeHist",gray)
    cv2.imshow("reflectance_img",reflectance_img)
    cv2.imshow("equalizeHistOFreflectance_img",gray2)
    if(cv2.waitKey(30)>0):
        break
#ACCESS_FAST