# USAGE
# python align_faces.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg

# import the necessary packages
from imutils.face_utils import FaceAligner
#from imutils.face_utils import rect_to_bb
#import argparse
import imutils
import dlib
import cv2
#import time
import numpy as np
cv2.namedWindow("Input",cv2.WINDOW_NORMAL)
cv2.namedWindow("Original",cv2.WINDOW_NORMAL)
cv2.namedWindow("Aligned",cv2.WINDOW_NORMAL)
cv2.namedWindow("diff",cv2.WINDOW_NORMAL)
cv2.namedWindow("eye_diff",cv2.WINDOW_NORMAL)
cv2.namedWindow("eye",cv2.WINDOW_NORMAL)
f=open("align_faces_log.txt","w")
f.close()
# construct the argument parser and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-p", "--shape-predictor", required=True,
# 	help="path to facial landmark predictor")
# ap.add_argument("-i", "--image", required=True,
# 	help="path to input image")
# args = vars(ap.parse_args())

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor and the face aligner
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")
fa = FaceAligner(predictor, desiredFaceWidth=256)

cam=cv2.VideoCapture(0)
#ret,image = cam.read()
#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
b=True
f=open("align_faces_log.txt","w")
f.close()
last_diff_img=np.zeros((256,256))
last_eye_point_var=0
ex=-1
while(1):
	try:
		# load the input image, resize it, and convert it to grayscale
		ret,image = cam.read()
		ret2,image2=cam.read()
		#image = imutils.resize(image, width=800)
		#image2 = imutils.resize(image2, width=800)
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


		# if b:
		# 	rects = detector(gray, 2)
		# 	for i in rects:
		# 		rect=i
		# 		b=False
		gray2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)
		# show the original input image and detect faces in the grayscale
		# image
		cv2.imshow("Input", image)
		#t1=time.time()

		#print("timetake by detector: ",time.time() - t1)
		#rects2 = detector(gray2, 2)

		face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
		eye_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_eye.xml')
		faces = face_cascade.detectMultiScale(gray, 1.3, 5)
		faces2 = face_cascade.detectMultiScale(gray2, 1.3, 5)


		# loop over the face detections
		for (x, y, w, h) in faces:
			# extract the ROI of the *original* face, then align the face
			# using facial landmarks
			#(x, y, w, h) = rect_to_bb(rect)
			faceOrig = imutils.resize(image[y:y + h, x:x + w], width=256)
			#t1=time.time()
			#rect=dlib.dlib.rectangles()
			#rect=rect[0]
			rect=dlib.rectangle(x, y, w+x, h+y)
			# rect.left=x
			# rect.top=y
			# rect.right=w+x
			# rect.bottom=h+y
			faceAligned = cv2.equalizeHist(fa.align(gray, gray[y:y + h, x:x + w], rect))
			eyes = eye_cascade.detectMultiScale(faceAligned)
			for i in eyes:
				if(ex==-1):
					ex=i[0]
				if(abs(i[0] - ex) < 20 ):
					(ex,ey,ew,eh)=i
					cv2.rectangle(faceAligned,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
					last_eye_point_var=ex
					break
				else:
					pass
			#print("timetake by align: ",time.time() - t1)
			#import uuid
			#f = str(uuid.uuid4())
			#cv2.imwrite("foo/" + f + ".png", faceAligned)

			# display the output images
			cv2.imshow("Original", faceOrig)
			cv2.imshow("Aligned", faceAligned)
			#if(cv2.waitKey(30)):
			#	break
		for (x, y, w, h) in faces2:
			# extract the ROI of the *original* face, then align the face
			# using facial landmarks=
			#(x, y, w, h) = rect_to_bb(rect)
			faceOrig = imutils.resize(image2[y:y + h, x:x + w], width=256)
			#t1=time.time()
			#rect=dlib.dlib.rectangles()
			#rect=rect[0]
			rect=dlib.rectangle(x, y, w+x, h+y)
			# rect.left=x
			# rect.top=y
			# rect.right=w+x
			# rect.bottom=h+y
			faceAligned2 = cv2.equalizeHist(fa.align(gray2, gray2[y:y + h, x:x + w], rect))
			eyes2 = eye_cascade.detectMultiScale(faceAligned2)
		print(faceAligned.shape)
		diff_img=cv2.absdiff(faceAligned2,faceAligned)
		eye_diff= (cv2.absdiff(cv2.equalizeHist(faceAligned2[ey:ey+eh, ex:ex+ew]),cv2.equalizeHist(faceAligned[ey:ey+eh, ex:ex+ew])) > 5).astype("float")
		cv2.imshow("eye",  cv2.equalizeHist(faceAligned[ey:ey+eh, ex:ex+ew]))
		cv2.imshow("eye_diff", eye_diff )
		cv2.imshow("diff", last_diff_img )
		last_diff_img=last_diff_img/2 + diff_img/2
		f=open("align_faces_log.txt","a+")
		f.write(str(np.mean(eye_diff)) + "\n")
		f.close()
	except Exception,e:
		print(str(e))
		pass
	if(cv2.waitKey(1)>0):
		break