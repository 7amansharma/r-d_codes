# USAGE
# python detect_face_parts.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg 

# import the necessary packages
from imutils import face_utils
import numpy as np
# import argparse
import imutils
import dlib
import cv2

cv2.namedWindow("Input",cv2.WINDOW_NORMAL)

predictor = dlib.shape_predictor("./shape_predictor_68_face_landmarks.dat")
#detector = dlib.get_frontal_face_detector()
cam=cv2.VideoCapture(0)
face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
mouth_ratio=-1
left_eye_ratio=-1
right_eye_ratio=-1
eye_ball_ratio=-1
f=open("mouth.txt","w")
#f.write(str(mouth_ratio) + "\n")
f.close()
f=open("left_eye.txt","w")
#f.write(str(left_eye_ratio) + "\n")
f.close()
f=open("right_eye.txt","w")
#f.write(str(right_eye_ratio) + "\n")
f.close()
f=open("eye_ball.txt","w")
f.close()
while(1):
	# load the input image, resize it, and convert it to grayscale
	ret,image = cam.read()
	if not ret:
		break 
	image = imutils.resize(image, width=500)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

	# detect faces in the grayscale image
	#rects = detector(gray, 1)
	
	faces = face_cascade.detectMultiScale(gray, 1.3, 5)
	if(len(faces) < 1):
		continue
	for (x,y,w,h) in faces:
		rect=dlib.rectangle(x,y,w+x,h+y)
		break
	i=0
	# loop over the face detections
	#for (i, rect) in enumerate(rects):
	# determine the facial landmarks for the face region, then
	# convert the landmark (x, y)-coordinates to a NumPy array
	shape = predictor(gray, rect)
	shape = face_utils.shape_to_np(shape)
	mouth_ratio=(float)(shape[54][0] - shape[48][0]) / (shape[57][1]- shape[51][1])
	left_eye_ratio=(float)(shape[39][0] - shape[36][0]) / (shape[38][1]- shape[40][1])
	right_eye_ratio=(float)(shape[45][0] - shape[42][0]) / (shape[44][1]- shape[46][1])
	#x=
	#print(x)
	rect_ll = cv2.boundingRect(np.array([[ [ shape[36][0],shape[36][1] ] ],[[shape[37][0],shape[37][1]]],[[shape[41][0],shape[41][1]]]]))
	#print("rect_ll",rect_ll)
	#box_ll = cv2.boxPoints(rect_ll)
	rect_lr = cv2.boundingRect(np.array([[ [ shape[38][0],shape[38][1] ] ],[[shape[39][0],shape[39][1]]],[[shape[40][0],shape[40][1]]]]))
	#rect_lr = cv2.boundingRect(np.array([shape[38],shape[39],shape[40]]))
	#box_lr = cv2.boxPoints(rect_ll)

	sum_ll=np.sum(gray[rect_ll[0]:rect_ll[2]+rect_ll[0],rect_ll[1]:rect_ll[1] + rect_ll[3] ])
	sum_lr=np.sum(gray[rect_lr[0]:rect_lr[2]+rect_lr[0],rect_lr[1]:rect_lr[1] + rect_lr[3] ])
	eye_ball_ratio = float(sum_ll)/(sum_lr)
	#print(eye_ball_ratio)
	f=open("eye_ball.txt","a+")
	f.write(str(eye_ball_ratio) + "\n")
	f.close()
	f=open("mouth.txt","a+")
	f.write(str(mouth_ratio) + "\n")
	f.close()
	f=open("left_eye.txt","a+")
	f.write(str(left_eye_ratio) + "\n")
	f.close()
	f=open("right_eye.txt","a+")
	f.write(str(right_eye_ratio) + "\n")
	f.close()
	cv2.imshow("Input",image)
	if(cv2.waitKey(30)>0):
		break
	# loop over the face parts individually
	# for (name, (i, j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
	# 	# clone the original image so we can draw on it, then
	# 	# display the name of the face part on the image
	# 	if((name=="mouth") or (name=="right_eye") or (name=="left_eye")):
	# 		clone = image.copy()
	# 		cv2.putText(clone, name, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
	# 			0.7, (0, 0, 255), 2)

	# 		# loop over the subset of facial landmarks, drawing the 
	# 		# specific face part
	# 		if(name=="mouth"):
	# 			mouth_ratio=(float)(shape[54][0] - shape[48][0]) / (shape[57][1]- shape[51][1])
	# 			#print(mouth_ratio)
	# 			#break
	# 			pass
	# 		elif(name=="right_eye"):
	# 			left_eye_ratio=(float)(shape[39][0] - shape[36][0]) / (shape[38][1]- shape[40][1])
	# 			pass
	# 		else:
	# 			right_eye_ratio=(float)(shape[45][0] - shape[42][0]) / (shape[44][1]- shape[46][1])
	# 			pass
	# 		for (x, y) in shape[i:j]:
	# 			cv2.circle(clone, (x, y), 1, (0, 0, 255), -1)

	# 		# extract the ROI of the face region as a separate image
	# 		(x, y, w, h) = cv2.boundingRect(np.array([shape[i:j]]))
	# 		roi = image[y:y + h, x:x + w]
	# 		roi = imutils.resize(roi, width=250, inter=cv2.INTER_CUBIC)

	# 		# show the particular face part
	# 		cv2.imshow("ROI", roi)
	# 		cv2.imshow("Image", clone)
	# 		cv2.waitKey(0)

	# 		# visualize all facial landmarks with a transparent overlay
	# 		output = face_utils.visualize_facial_landmarks(image, shape)
	# 		cv2.imshow("Image", output)
	# 		cv2.waitKey(0)
	#break